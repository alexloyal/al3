<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package alejandroleal
 */
?>

	</div><!-- #content -->
	
	<div id="backToTop" class="fixed bottom right">
		<span><a href="#" class="arrow up">Back to top</a></span>
	</div>	

	<footer id="colophon" class="site-footer row" role="contentinfo">
		<p class="coleccionista">coleccionista de buenas ideas</p>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>