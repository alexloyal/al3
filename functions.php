<?php
/**
 * alejandroleal functions and definitions
 *
 * @package alejandroleal
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'alejandroleal_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function alejandroleal_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on alejandroleal, use a find and replace
	 * to change 'alejandroleal' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'alejandroleal', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'alejandroleal' ),
		'footer' => __('Footer Menu','alejandroleal')
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'alejandroleal_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // alejandroleal_setup
add_action( 'after_setup_theme', 'alejandroleal_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function alejandroleal_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'alejandroleal' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Page Sidebar', 'alejandroleal' ),
		'id'            => 'sidebar-page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'alejandroleal_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function alejandroleal_scripts() {
	wp_enqueue_style( 'alejandroleal-style', get_stylesheet_uri() );
	
	wp_enqueue_style('34gs-queries', get_template_directory_uri() . '/layouts/34gs-queries.css');


	wp_enqueue_script('mousewheel-plugin', get_template_directory_uri() . '/js/fancybox/jquery.mousewheel-3.0.6.pack.js', array(), '20140413',true );
	wp_enqueue_script('fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', array(), '20140413',true );

	wp_enqueue_script('fancybox-thumbs', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-thumbs.js', array(), '20140413',true );

	wp_enqueue_script('fancybox-media', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-media.js', array(), '20140413',true );

	wp_enqueue_script('fancybox-buttons', get_template_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-buttons.js', array(), '20140413',true );
	

	wp_enqueue_script( 'alejandroleal-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'alejandroleal-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	
	wp_enqueue_script('triggers',get_template_directory_uri() . '/js/triggers.js', array(), '20140105', true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'alejandroleal_scripts' );

function remove_more_link_scroll( $link ) {
	$link = preg_replace( '|#more-[0-9]+|', '', $link );
	return $link;
}
add_filter( 'the_content_more_link', 'remove_more_link_scroll' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */


require get_template_directory() . '/inc/custom-post-types.php';

/* Theme options */

require get_template_directory() . '/inc/theme-options.php';



require get_template_directory() . '/inc/gallery-shortcode.php';
