<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package alejandroleal
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">	
<link href='http://fonts.googleapis.com/css?family=Nixie+One|Gudea:400,700,400italic' rel='stylesheet' type='text/css'>
<link rel="apple-touch-icon" href="touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo( 'template_url' ); ?>/images/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo( 'template_url' ); ?>/images/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo( 'template_url' ); ?>/images/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo( 'template_url' ); ?>/images/touch-icon-ipad-retina.png">
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/files/css3-mediaqueries.js"></script>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class('container'); ?>>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	<header id="masthead" class="site-header row" role="banner">
		<div id="branding" class="site-branding">
			<h1 id="site-title" class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		</div>


	</header><!-- #masthead -->

<section class="row">	
	<nav id="site-navigation" class="top-menu nav" role="navigation">
		
		<?php wp_nav_menu( array( 'theme_location' => 'primary','container_id' => 'mainMenu', ) ); ?>
	</nav><!-- #site-navigation -->

</section>

<section id="secondary-navigation" class="row">
	<div id="navHomeLink"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></div>
	<?php wp_nav_menu( array( 'theme_location' => 'primary','container_id' => 'secondMenu', ) ); ?>
</section>
	<div id="content" class="site-content row">
