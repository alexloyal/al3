<?php
	
add_action('init', 'experience_register');  

function experience_register() {
    $args = array(
        'label' => __('Experience'),
        'singular_label' => __('Role'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'experience' ),
        'has_archive'=> true,
        
        'supports' => array('title', 'editor', 'thumbnail','excerpt','custom-fields','post-formats')
       );  

    register_post_type( 'experience' , $args );
}

add_action("admin_init", "experience_meta_box");   

			function experience_meta_box(){
			    add_meta_box("job_details_meta", "Job Details", "experience_meta_options", "experience", "side", "low");
			}  

			function experience_meta_options(){
			        global $post;
			        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
			        $custom = get_post_custom($post->ID);
			        $job_title = $custom["job_title"][0];
			        $job_city = $custom["job_city"][0];
			        $job_tenure = $custom["job_tenure"][0];
			        $job_url = $custom["job_url"][0];
			?>
			<label>Job Title</label><br><input name="job_title" value="<?php echo $job_title; ?>" /><br /><br />
			<label>Job City</label><br><input name="job_city" value="<?php echo $job_city; ?>" /><br /><br />
			<label>Term Length</label><br><input name="job_tenure" value="<?php echo $job_tenure; ?>" /><br /><br />
			<label>Org. URL</label><br><input name="job_url" value="<?php echo $job_url; ?>" />
			<?php
			    }

				add_action('save_post', 'save_job_title'); 
				add_action('save_post', 'save_job_city'); 
				add_action('save_post', 'save_job_tenure');
				add_action('save_post', 'save_job_url'); 

				function save_job_title(){
				    global $post;  

				    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
						return $post_id;
					}else{
				    	update_post_meta($post->ID, "job_title", $_POST["job_title"]);
				    }
				}

				function save_job_city(){
				    global $post;  

				    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
						return $post_id;
					}else{
				    	update_post_meta($post->ID, "job_city", $_POST["job_city"]);
				    }
				}

				function save_job_tenure(){
				    global $post;  

				    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
						return $post_id;
					}else{
				    	update_post_meta($post->ID, "job_tenure", $_POST["job_tenure"]);
				    }
				}

				function save_job_url(){
					global $post;

					if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
						return $post_id;
					}else{
						update_post_meta($post->ID,"job_url", $_POST["job_url"]);

					}
				}
				 

// FIX ABOVE, FINISH ADDING JOB TITLE CUSTOM META BOX TO EXPERIENCE POST TYPE 				

register_taxonomy("experience-type", 
				array("experience"), 
				array(
					"hierarchical" => true, 
					"label" => "Experience Type", 
					"singular_label" => "Experience Type", 
					"rewrite" => true
					)
);

register_taxonomy("organization", 
				array("experience","portfolio"), 
				array(
					"hierarchical" => false, 
					"label" => "Organization", 
					"singular_label" => "organization", 
					"rewrite" => true
					)
);

// BEGIN

function organization_taxonomy_custom_fields($tag) { 

	$t_id = $tag->term_id;
	$term_meta = get_option( "taxonomy_term_$t_id");

	?>

	<tr class="form-field">
			<th scope="row" valign="top">
					<label for="org_url"><?php _e('Organization URL'); ?></label>
			</th>
			<td>
				<input type="text" name="term_meta[org_url]" id="term_meta[org_url]"size="25" style="width:60%;" value="<?php echo $term_meta['org_url'] ? $term_meta['org_url'] : ''; ?>"><br /><br />


			</td>

	</tr>

	<tr>
			<th scope="row" valign="top">
					<label for="org_logo"><?php _e('Organization Logo URL'); ?></label>
			</th>
			<td>
			<input type="text" name="term_meta[org_logo]" id="term_meta[org_logo]"size="25" style="width:60%;" value="<?php echo $term_meta['org_logo'] ? $term_meta['org_logo'] : ''; ?>"><br /><br />

			</td>

	</tr>
	<?php 

}

// A callback function to save our extra taxonomy field(s)
function save_taxonomy_custom_fields( $term_id ) {
    if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_term_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
            foreach ( $cat_keys as $key ){
            if ( isset( $_POST['term_meta'][$key] ) ){
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        //save the option array
        update_option( "taxonomy_term_$t_id", $term_meta );
    	}
  }



// Add the fields to the "organization" taxonomy, using our callback function
add_action( 'organization_edit_form_fields', 'organization_taxonomy_custom_fields', 10, 2 );
add_action( 'organization_add_form_fields', 'organization_taxonomy_custom_fields', 10, 2 );

	
// Save the changes made on the "organization" taxonomy, using our callback function
add_action( 'edited_organization', 'save_taxonomy_custom_fields', 10, 2 );
add_action( 'create_organization', 'save_taxonomy_custom_fields', 10, 2 );



// STOP

add_action('init', 'portfolio_register');

function portfolio_register() {
    $args = array(
        'label' => __('Portfolio'),
        'singular_label' => __('Project'),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'projects' ),
        'has_archive'=> true,
        
        'supports' => array('title', 'editor', 'thumbnail','excerpt','custom-fields','post-formats')
       );  

    register_post_type( 'portfolio' , $args );
}

register_taxonomy("project-type", 
				array("portfolio"), 
				array(
					"hierarchical" => true, 
					"label" => "Project Types", 
					"singular_label" => "Project Type", 
					"rewrite" => true
					)
);

			add_action("admin_init", "portfolio_meta_box");   

			function portfolio_meta_box(){
			    add_meta_box("projInfo-meta", "Project Options", "portfolio_meta_options", "portfolio", "side", "low");
			}  

			function portfolio_meta_options(){
			        global $post;
			        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
			        $custom = get_post_custom($post->ID);
			        $link = $custom["projLink"][0];
			?>
			    <label>Link: </label><input name="projLink" value="<?php echo $link; ?>" />
			<?php
			    }

				add_action('save_post', 'save_project_link'); 

				function save_project_link(){
				    global $post;  

				    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
						return $post_id;
					}else{
				    	update_post_meta($post->ID, "projLink", $_POST["projLink"]);
				    }
				}
				add_filter("manage_edit-portfolio_columns", "project_edit_columns");   

				function project_edit_columns($columns){
				        $columns = array(
				            "cb" => "<input type=\"checkbox\" />",
				            "title" => "Project",
				            "description" => "Description",
				            "link" => "Project Link",
				            "type" => "Type of Project",
				        );  

				        return $columns;
				}  

				// Custom column for admin custom post type browse page.

				add_action("manage_posts_custom_column",  "project_custom_columns"); 

				function project_custom_columns($column){
				        global $post;
				        switch ($column)
				        {
				            case "description":
				                the_excerpt();
				                break;
				            case "link":
				                $custom = get_post_custom();
				                echo $custom["projLink"][0];
				                break;
				            case "type":
				                echo get_the_term_list($post->ID, 'project-type', '', ', ','');
				                break;
				        }
				}
	
?>