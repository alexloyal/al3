<?php

add_action( 'admin_menu', 'theme_options_menu' );

function register_mysettings() { // whitelist options
  register_setting( 'al3_options_group', 'new_option_name' );
  register_setting( 'myoption-group', 'some_other_option' );
  register_setting( 'myoption-group', 'option_etc' );
}

function theme_options_menu() {
	add_theme_page( 'Options', 'AL3 Options', 'manage_options', 'al3-theme-options', 'theme_options' );
}

 
function theme_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap">';
	echo '<h1>Home Page Configuration</h1>';
	echo '<p>Here is where the form would go if I actually had options.</p>';
	echo '</div>'; ?>
	
	<form method="post" action="options.php"> 
		
		<?php settings_fields( 'al3_options_group' );
			  do_settings_sections( 'al3_options_group' );
		 ?>
		
		<p class="optionsLabel">About</p>
		
		<textarea rows="12" cols="50"></textarea>
		
		<?php submit_button(); ?>
	</form>
	
	<?php 
}



?>