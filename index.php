<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package alejandroleal
 */

get_header(); ?>

<section id="tagline" class="row">
 
 

		<?php 
						
					$aboutArgs = array (
								
						);
						$about_query = new WP_Query('pagename=homepage-intro'); ?>
			
			
					<?php if ($about_query->have_posts()) : 
						while ($about_query->have_posts()) : 
							$about_query->the_post(); ?>
							
							<h3> 
									<?php 
										$content = get_the_content(''); 
										print $content;
									?>
							 
							</h3>
							
						
					<?php endwhile; ?>
					
					<?php wp_reset_postdata(); ?>
				 
					<?php endif;  ?>

 
	
	
</section>

<section id="work" class="row">
	<h3 class="lead">Recent Projects</h3>
	<?php
				
					//The Query
					if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
					elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
					else { $paged = 1; }
					$args=array(
					'paged'=>$paged, //Pulls the paged function into the query
					'posts_per_page'=>12, //Limits the amount of posts on each page
					'post_type'=>'portfolio'
					);
					$portfolio_query = new WP_Query($args);


					?>
			
			
					<?php if ($portfolio_query->have_posts()) : 
						while ($portfolio_query->have_posts()) : 
							$portfolio_query->the_post(); ?>

						<?php
							//$title= str_ireplace('"', '', trim(get_the_title()));
							// $desc= str_ireplace('"', '', trim(get_the_content()));
							$title= strip_tags(get_the_title());
							$desc= strip_tags(get_the_excerpt());
						?>	

					<article class="col_6">
						<div class="project">
								<div class="projectImage">
										<a title="<?php echo $title; ?>: <?php echo $desc; ?>" rel="portfolio" href="<?php the_permalink();	 
											// print  portfolio_thumbnail_url($post->ID) ?>">
										<?php the_post_thumbnail('thumbnail'); ?></a>
								</div>		
										<h3><a title="<?php echo $title; ?>: <?php  echo $desc; ?>" rel="lightbox[work]" href="<?php //print  portfolio_thumbnail_url($post->ID)
											the_permalink(); ?>">
											<?php echo $title; ?></a></h3> 
									<p><?php print get_the_excerpt(); ?> <a title="<?php echo $title; ?>: <?php echo $desc; ?>" rel="lightbox[work]" href="<?php //print  portfolio_thumbnail_url($post->ID)
									the_permalink(); ?>"></a></p>
									<?php $site= get_post_custom_values('projLink');
										if($site[0] != ""){

									?>
										<p><a href="<?php echo $site[0]; ?>" target="_blank" class="greenLink">Visit project site &raquo;</a></p>

									<?php }else{ ?>
										<p><em>No link available</em></p>
									<?php } ?>
							</div>
					</article><!-- project -->
					
					<!-- <div class="navigation grid_12">	
						<ul id="postPagination">
							<li class="alignright"><a href="#">See other selected projects &raquo;</a></li>
						</ul>
					</div>-->

					<?php endwhile; wp_reset_postdata();  ?>
						
					<?php endif; ?>
	
	
	 
		
		 
	  
	
	
</section><!-- Work History -->



<section id="experience" class="row">
	<h3 class="lead">Professional Experience</h3>
	<?php
				
					//The Query
					if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
					elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
					else { $paged = 1; }
					$args=array(
					'paged'=>$paged, //Pulls the paged function into the query
					'posts_per_page'=>12, //Limits the amount of posts on each page
					'post_type'=>'experience'
					);
					$experience_query = new WP_Query($args);


					?>
			
			
					<?php if ($experience_query->have_posts()) : 
						while ($experience_query->have_posts()) : 
							$experience_query->the_post(); ?>

						<?php
							//$title= str_ireplace('"', '', trim(get_the_title()));
							// $desc= str_ireplace('"', '', trim(get_the_content()));
							$title= strip_tags(get_the_title());
							$desc= strip_tags(get_the_excerpt());
						?>	

					<article class="col_3">
						<div class="project">
										
							<h3>
								<a title="<?php echo $title; ?>: <?php echo $desc; ?>" rel="experience" href="<?php the_permalink();	 
											// print  experience_thumbnail_url($post->ID) ?>">
										<?php the_post_thumbnail('thumbnail'); ?></a>

								<a title="<?php echo $title; ?>: <?php  echo $desc; ?>" rel="lightbox[work]" href="<?php //print  experience_thumbnail_url($post->ID)
								the_permalink(); ?>">
								<?php echo $title; ?></a>
							</h3>
				
							
							<p>
					
								<?php 
									$job_city = get_post_custom_values('job_city'); 
									$job_tenure = get_post_custom_values('job_tenure');
									$job_title= get_post_custom_values('job_title');
									$job_url= get_post_custom_values('job_url');
								?>	  
								<label data-jobtenure="<?php echo$job_tenure[0]; ?>" class="projectMeta">
									<?php echo $job_tenure[0]; ?>
								</label><br />
								<label data-jobtitle="<?php echo$job_title[0]; ?>" class="projectMeta">
									<b><?php echo $job_title[0]; ?></b><br />
								</label>
								
								<label data-jobcity="<?php echo$job_city[0]; ?>" class="projectMeta">
									<?php echo $job_city[0]; ?><br />
								</label>
								<label class="projecMeta">
									<a href="<?php echo $job_url[0]; ?>" target="_blank" title="<?php echo $title; ?>">

										<?php 
											
											// in case scheme relative URI is passed, e.g., //www.google.com/
											$job_url[0] = trim($job_url[0], '/');

											// If scheme not included, prepend it
											if (!preg_match('#^http(s)?://#', $job_url[0])) {
											$job_url[0] = 'http://' . $job_url[0];
											}

											$urlParts = parse_url($job_url[0]);

											// remove www
											$cleanUrl = preg_replace('/^www\./', '', $urlParts['host']);

											
										echo $cleanUrl; ?></a>
								</label>
							</p> 
							<p>
								 	
								
								<?php print get_the_excerpt(); ?>
							<a title="<?php echo $title; ?>: <?php echo $desc; ?>" rel="lightbox[experience]" href="<?php //print  experience_thumbnail_url($post->ID)
					the_permalink(); ?>"></a></p>


									
							</div>
					</article><!-- project -->
					
					<!-- <div class="navigation grid_12">	
						<ul id="postPagination">
							<li class="alignright"><a href="#">See other selected projects &raquo;</a></li>
						</ul>
					</div>-->

					<?php endwhile; wp_reset_postdata();  ?>
						
					<?php endif; ?>
	
	
	 
		
		 
	  
	
	
</section><!-- Work History -->

	
<section id="bio" class="row">
	
					<?php 
						
					$aboutArgs = array (
								
						);
						$about_query = new WP_Query('pagename=bio'); ?>
			
			
					<?php if ($about_query->have_posts()) : 
						while ($about_query->have_posts()) : 
							$about_query->the_post(); ?>
							
							<h3 class="lead"><?php the_title(); ?></h3>
							
							<article class="col_1" id="about">
								<div class="inner">
									<?php the_content('<h3>Detailed bio and contact info &raquo;</h3>'); ?>
								</div>	
							</article>
							
						
					<?php endwhile; ?>

					<article class="col_1" id="footerMenu">
						<?php wp_nav_menu( array( 'theme_location' => 'footer','container_class' => 'inner', 'container_id' => '', ) ); ?>
					</article>
					
					<?php wp_reset_postdata(); ?>
				 
					<?php endif;  ?>
		
		
</section><!-- about -->



<?php get_footer(); ?>
