// Resize elements to height of viewport

(function($) {
	$(document).ready(function(){
	  windowHeight = ($(window).height())*70/100;
	  $('#work').css('min-height', windowHeight);
	  $('#projects').css('min-height', windowHeight);
	  $('#tagline').css('min-height', windowHeight);	  
	});

	$(window).resize(function(){
	  windowHeight = ($(window).height())*70/100;
	  $('#work').css('min-height', windowHeight);
	  $('#projects').css('min-height', windowHeight);
	  $('#tagline').css('min-height', windowHeight);	  
	});
	
	$(window).scroll(function(){
		var scrollTop = $(window).scrollTop();
		if (scrollTop >= 216) {
			$('#fixedMenu').css('top','5%');
			$('#fixedMenu').addClass('fixed');
			$('#menu-item-569').addClass('menuTitle')
		} else if (scrollTop <= 217) {
			$('#fixedMenu').css('top','-100%');
			$('#fixedMenu').removeClass('fixed');
			$('#menu-item-569').removeClass('menuTitle')
		}
	});

// Smooth scroll	
	
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 560);
	        return false;
	      }
	    }
	  });
	});

// Makes all menu items equal width.

	$(document).ready(function() {
		menuWidth = $('.menu').width();
		menuItemWidth = (menuWidth / 3);
		$('.menu li a').css('width', menuItemWidth)
		
	});

	$(window).resize(function() {
		menuWidth = $('.menu').width();
		menuItemWidth = (menuWidth / 3);
		$('.menu li a').css('width', menuItemWidth)
		
	});
	
// Makes all Portfolio items same height.

	$(document).ready(function() {
	   var PmaxHeight = -1;

	   $('#work article').each(function() {
	     PmaxHeight = PmaxHeight > $(this).height() ? PmaxHeight : $(this).height();
	     PmaxHeight = PmaxHeight;
	   });

	   $('#work article').each(function() {
	     $(this).css('min-height', PmaxHeight);
	   });
	 });

	$(document).ready(function() {
	   var EmaxHeight = -1;

	   $('#experience article').each(function() {
	     EmaxHeight = EmaxHeight > $(this).height() ? EmaxHeight : $(this).height();
	     EmaxHeight = EmaxHeight;
	   });

	   $('#experience article').each(function() {
	     $(this).css('min-height', EmaxHeight);
	   });
	 });


	$(window).resize(function() {
	   var PmaxHeight = -1;

	   $('#work article').each(function() {
	     PmaxHeight = PmaxHeight > $(this).height() ? PmaxHeight : $(this).height();
	     PmaxHeight = PmaxHeight;
	   });

	   $('#work article').each(function() {
	     $(this).css('min-height', PmaxHeight);
	   });
	 });

	$(window).resize(function() {
	   var EmaxHeight = -1;

	   $('#experience article').each(function() {
	     EmaxHeight = EmaxHeight > $(this).height() ? EmaxHeight : $(this).height();
	     EmaxHeight = EmaxHeight;
	   });

	   $('#experience article').each(function() {
	     $(this).css('min-height', EmaxHeight);
	   });
	 });	


// Back to top script
	 
	 $(document).ready(function() {
				 $('#backToTop').hide();
				 
				 $(function () {
					 $(window).scroll(function() {
						 if ($(this).scrollTop() > 100){
							 $('#backToTop').fadeIn(); 
						 } else {
							 $('#backToTop').fadeOut();
						 }				 
					 });

					 	$(window).scroll(function(){
							var scrollTop = $(window).scrollTop();
							if (scrollTop >= 420) {
							var windowWidth = $(window).width();	
							$('#secondary-navigation').fadeIn();
						 	$('#secondary-navigation').css('top','0%');
						 	$('#secondary-navigation').addClass('fixed');
						 	// $('#secondary-navigation').css('width', windowWidth)
							} else if (scrollTop <= 419) {
								$('#secondary-navigation').fadeOut();
						 	$('#secondary-navigation').css('top','-25%');
						 	$('#secondary-navigation').removeClass('fixed')
							}
						});


					 
					 $('#backToTop a').click(function(){
						 $('body,html').animate({
							 scrollTop:0
						 }, 800);
						 return false;
					 });
				 });
	 });

	 // Fancybox
	 
	 $(document).ready(function() {
		$("a[href$='.jpg'],a[href$='.png'],a[href$='.gif']").attr('rel', 'gallery').fancybox({
			padding:10
		});
	});



	
})(jQuery);