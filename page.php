<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package alejandroleal
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="screen">
			<main id="main" class="site-main col_3c" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>
 

				<?php endwhile; // end of the loop. ?>

				

			</main><!-- #main -->
		<?php get_sidebar('page'); ?>	
		</div>
	</div><!-- #primary -->


<?php get_footer(); ?>
