<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package alejandroleal
 */
?>
	<div id="secondary" class="widget-area col_4" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		<?php if ( ! dynamic_sidebar( 'sidebar-page' ) ) : ?>

			<aside id="search" class="widget widget_search">
				<?php get_search_form(); ?>
			</aside>

			

		<?php endif; // end sidebar widget area ?>
	</div><!-- #secondary -->
