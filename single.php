<?php
/**
 * The Template for displaying all single posts.
 *
 * @package alejandroleal
 */

get_header(); ?>

	<div id="primary" class="content-area row">
		<div class="screen">
			<main id="main" class="site-main col_3c" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single' ); ?>

			<?php endwhile; // end of the loop. ?>
		
			</main><!-- #main -->
			<?php get_sidebar(); ?>
		</div>
	</div><!-- #primary -->

	

<?php get_footer(); ?>